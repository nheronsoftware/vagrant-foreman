#!/bin/bash


wget  http://apt.puppetlabs.com/puppetlabs-release-precise.deb
dpkg -i puppetlabs-release-precise.deb

sudo apt-get update

sudo apt-get -y install puppet

echo "deb http://deb.theforeman.org/ precise 1.5" > /etc/apt/sources.list.d/foreman.list
echo "deb http://deb.theforeman.org/ plugins 1.5" >> /etc/apt/sources.list.d/foreman.list
wget -q http://deb.theforeman.org/foreman.asc -O- | apt-key add -
apt-get update
apt-get -y install foreman-installer


hostname foreman.home
echo "foreman.home" > /etc/hostname
sed -i -e "s/127.0.1.1 foreman foreman/127.0.1.1 foreman.home/g" /etc/hosts
sudo foreman-installer
